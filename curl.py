from loguru import logger

from flexget import plugin
from flexget.event import event

logger = logger.bind(name='curl')


class OutputCurl:
    """
    Example::

      curl:
        url: https://gitlab.com/api/v4/projects/PROJECT_ID/trigger/pipeline
    """

    schema = {
        'type': 'object',
        'properties': {
            'url': {'type': 'string'},
        },
        'required': ['url'],
        'additionalProperties': False,
    }

    def on_task_output(self, task, config):
        import requests
        import base64
        import os
        import re

        params = dict(config)
        server = params["url"]

        for entry in task.accepted:

            # Magnet link base64 encoded
            encodedBytes = base64.b64encode(entry['url'].encode("utf-8"))
            encoded_link = str(encodedBytes, "utf-8")

            # Cleanup title
            try:
                p = re.compile('([\w\s]+S\d\dE\d\d)')
                title = p.match(entry['title']).group().replace(" ", "_")
            except Exception as e:
                logger.error('Could not regexp, just replacing spaces: {}', e)
                title = entry['title'].replace(" ", "_")

            # When running on CI
            try:
                token = os.environ['CI_JOB_TOKEN']
            except Exception as e:
                logger.error('Can not find CI_JOB_TOKEN: {}', e)


            if task.options.test:
                logger.info('Would add: {}', title)
                continue

            try:
                requests.post(
                    server,
                    data={
                        'token':token,
                        'ref':'master',
                        'variables[LINK]': encoded_link,
                        'variables[TITLE]': title,
                        'variables[SCHEDULED]': 'FALSE',
                    }
                )
            except Exception as e:
                logger.critical('rpc call to url failed: {}', e)
                entry.fail('could not call appendurl via RPC')


@event('plugin.register')
def register_plugin():
    plugin.register(OutputCurl, 'curl', api_ver=2)
