# hadolint ignore=DL3007
FROM alpine:latest
LABEL version="0.0.1"

# hadolint ignore=DL3018
RUN apk add --no-cache \
  python3 \
  py3-pip \
  ca-certificates \
  curl

# hadolint ignore=DL3013
RUN pip3 install -U pip \
      paver \
      flexget \
      transmissionrpc

COPY curl.py /usr/lib/python3.8/site-packages/flexget/plugins/clients/curl.py
COPY template.yml /template.yml
